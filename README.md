# Terraform - Provision a GKE Cluster with GitLab CI/CD pipeline and deploy a Wordpress App

## Requisites
 - Install gcloud SDK
 - Install Terraform
 - Install kubectl

## K8s Cluster Setup

 1. Initialize and authenticate with the gcloud SDK 
    ```bash
    gcloud init
    ```
    At the end of the setup it will ask for a project name. I used: gcloud-gke-terraform-demo. I also set that as the current project with the command:
 
    ```bash
    gcloud config set project gcloud-gke-terraform-demo
    ```

 2. (Local execution) Add your account to the Application Default Credentials (ADC). This will allow Terraform to access these credentials to provision resources on GCloud.

    ```bash
    gcloud auth application-default login
    ```
 2. (Automated GitLab CI execution) Create a service account in GCP named gke-terraform-demo with the compute and container Roles. Once done, choose create key and export as JSON. Save the content of that file encoded in Base64 as a secure variable in GitLab variables for future use in the pipeline.

 3. Google Cloud Compute and container APIs must be enabled in order to allow Terraform create related resources
    
    ```bash
    gcloud services enable compute.googleapis.com
    gcloud services enable container.googleapis.com
    ```

 4. Generate kubeconfig file for kubectl
    ```bash
    gcloud container clusters get-credentials $(terraform output -raw kubernetes_cluster_name) --region $(terraform output -raw region)
    ```

## Improvements to be done

 - Use a remote state storage such as GCS (Google Cloud Storage) or GitLab managed Terraform State
 - Setup configuration to run and deploy different environments
 - Setup configuration manager
 - Use Terragrunt as a Terraform wrapper to cover all of the above improvements
